#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase


## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

# test macros
#epicsEnvSet("IOCPREFIX", "LPQ:SYNCLINO:")
#epicsEnvSet("DEVIP", "10.42.0.152")
#epicsEnvSet("DEVPORT", "23")

## macros
epicsEnvSet("P", "$(IOCPREFIX)")
epicsEnvSet("PORT", "L1")
epicsEnvSet("STREAM_PROTOCOL_PATH", "${TOP}/iocBoot/${IOC}/.")

drvAsynIPPortConfigure("L1","$(DEVIP):$(DEVPORT)",0,0,0)

## Load record instances
dbLoadRecords("${ASYN}/db/asynRecord.db","P=$(P),R=asyn,PORT=$(PORT),ADDR=0,IMAX=256,OMAX=256")


cd "${TOP}/iocBoot/${IOC}"
dbLoadRecords("synclino.db","P=$(P), PORT=$(PORT)")
iocInit

## Start any sequence programs
#seq sncxxx,"user=epics"
